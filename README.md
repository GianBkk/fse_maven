# Maven Documention

## MVN

Maven ist ein build tools. Das es vereinfacht Dependencies zu installieren.

## POM

Die Pom.xml ist das herz eines Maven projekts. Es enthält informationen verpflichtend um das Projekt zu bauen. Hier stehen Bsp. die Dependencies und Plugins.

## Lifecycles

Maven hat einige default Lifecycles.

- validate: validate the project is correct and all necessary information is available
- compile: compile the source code of the project
- test: test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed
- package: take the compiled code and package it in its distributable format, such as a JAR.
- integration-test: process and deploy the package if necessary into an environment where integration tests can be run
- verify: run any checks to verify the package is valid and meets quality criteria
- install: install the package into the local repository, for use as a dependency in other projects locally
- deploy: done in an integration or release environment, copies the final package to the remote repository for sharing with other developers and projects.

Es gibt auch einige nicht default Lifecycles:

- clean: cleans up artifacts created by prior builds
- site: generates site documentation for this project

## Spring Boot

Spring Boot ist das am meist verwendete Java Rest Framework.
Es dient dazu Rest APIs zu machen. Es funktioiert mit maven und gradle.

Selber habe ich Spring schon verwendet.

<https://github.com/GianBkk/einkaufswagen>
<https://github.com/GianBkk/spring-app>

